#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int file_info (const char * name, int *count_files, int * total_strings, int * total_words, int * total_symbols){
    if (access(name, F_OK)<0){
       if (printf("%s: such file doesn't exist.\n", name)<0){
           perror("Cannot write to output.\n");
           exit(1);
       }
       return;
    }
    FILE * f = fopen(name, "r");
    if (!f){
        perror("Cannot open the file.\n");
        exit(1);
    }
    int strings = 0;
    int words = 0;
    int symbols = 0;
    char flag_word = 1;
    int buffer;
    while (EOF!=(buffer = fgetc(f))){
        ++symbols;
        if (buffer == '\n')
            ++strings;
        if (flag_word && !isspace(buffer)){
            flag_word = 0;
            ++words;
        }
        else if(!flag_word && isspace(buffer))
            flag_word = 1;
    }
    if (ferror(f)){
        perror("Error while reading file.\n");
        exit(1);
    }
    if (printf("%d %d %d %s\n", strings, words, symbols, name)<0){
        perror("Cannot write to output.\n");
        exit(1);
    }
    if (fclose(f)<0){
        perror("Cannot close the file.\n");
        exit(1);
    }
    ++(*count_files);
    *total_strings+=strings;
    *total_words+=words;
    *total_symbols+=symbols;
}

int main(int argc, char *argv[]){
    if (argc==1){
        int strings = 0;
        int words = 0;
        int symbols = 0;
        char flag_word = 1;
        int buffer;
        while (EOF!=(buffer = getchar())){
            ++symbols;
            if (buffer == '\n')
                ++strings;
            if (flag_word && !isspace(buffer)){
                flag_word = 0;
                ++words;
            }
            else if(!flag_word && isspace(buffer))
                flag_word = 1;
        }
        if (ferror(stdin)){
            perror("Error while reading output.\n");
            exit(1);
        }
        if (printf("%d %d %d\n", strings, words, symbols)<0){
            perror("Cannot write to output.\n");
            exit(1);
        }
    }
    else{
        int i;
        int count_files = 0;
        int total_strings = 0;
        int total_words = 0;
        int total_symbols = 0;
        for (i = 1; i<argc; ++i)
            file_info(argv[i], &count_files, &total_strings, &total_words, &total_symbols);
        if (count_files>1)
            if (printf("%d %d %d total\n", total_strings, total_words, total_symbols)<0){
                perror("Cannot write to output.\n");
                exit(1);
            }
    }
    return 0;
}

#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#define BUFFSIZE 10000

void write_file(const char * name){
    if (access(name, F_OK)<0){
        if (write(STDOUT_FILENO, name, strlen(name))<0){
            perror("Cannot write to output.\n");
            exit(1);
        }
        if (write(STDOUT_FILENO, ":such file doesn't exist.\n", strlen(":such file doesn't exist.\n"))<0){
            perror("Cannot write to output.\n");
            exit(1);
        }
        return;
    }

    int fd = open(name, O_RDONLY);
    if (fd < 0){
        perror("Cannot open the file.\n");
        exit(1);
    }
    ssize_t ret;
    char ch;
    while ((ret = read (fd, &ch, 1))>0)
        if (write(STDOUT_FILENO, &ch, 1)<0){
            perror("Cannot write to standart output.\n");
            exit(1);
        }
    if (ret<0){
        perror("Cannot read the file.\n");
        exit(1);
    }
    if (close (fd)<0){
        perror("Failure of closing file.\n");
        exit(1);
    }
}

void bufferize(){
    char buffer[BUFFSIZE];
    ssize_t ret;
    while ((ret = read (STDIN_FILENO, buffer, BUFFSIZE))>0){
       if (write(STDOUT_FILENO, buffer, ret)<0){
            perror("Cannot write to the output.\n");
            exit(1);
        }
    }
    if (ret<0){
        perror("Cannot read from the input.\n");
        exit(1);
    }
}

void wobufferize(){
    char ch;
    ssize_t ret;
    while ((ret = read (STDIN_FILENO, &ch, 1))>0)
        if (write(STDOUT_FILENO, &ch, 1)<0){
            perror("Cannot write to the output.\n");
            exit(1);
        }
    if (ret<0){
        perror("Cannot read from the input.\n");
        exit(1);
    }
}

int main(int argc, char *argv[])
{
  if (argc == 1){
      bufferize();
      return 0;
  }
  if (strcmp(argv[1], "-u")!=0){
       int i;
       for (i = 1; i<argc; ++i){
           write_file(argv[i]);
       }
  }
  else if (argc == 2)
      wobufferize();
  else {
      int i;
      for (i = 2; i<argc; ++i){
          write_file(argv[i]);
      }
  }
  return 0;
}

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>



void child_proc (int in, int out, char *command){
    int k = fork();
    if (k<0){
        perror("fork");
        exit(1);
    }
    if (k==0){
        if (in != 0){
            dup2 (in, 0);
            close (in);
        }
        if (out != 1){
            dup2 (out, 1);
            close (out);
        }
        execlp(command, command, NULL);
        perror(command);
        exit(1);
    }
}


int main (int argc, char **argv){
    if (argc < 1)
        return 0;
    int i, in, n;
    int fd[argc-1][2];
    fd[0][0] = 0;
    in = 0;
    for (i = 1; i < argc ; ++i){
        if (i!=argc-1)
            pipe (fd[i]);
        else
            fd[i][1] = 1;
        child_proc(fd[i-1][0], fd[i][1], argv[i]);
        if (i!=1)
            close(fd[i-1][0]);
        close(fd[i][1]);
    }
    for (i = 1; i < argc; ++i)
        wait (&n);
    return 0;
}


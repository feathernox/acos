#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int cmp (const void *a, const void *b)
{
	return(strcmp(*(char**)a, *(char**)b));
}

void error_f()
{
	perror(NULL);
	exit (1);
}

void add_word(char **p, int words, int letter, char *tmp)
{
	p[words] = (char *) malloc ((letter+1) * sizeof(char));
	if (!p[words])
		error_f();
	int i;
	for (i = 0; i<letter; ++i)
		p[words][i]=tmp[i];
	p[words][letter] = 0;
}

void check_flag(int * flag, char **p, int words, int letter, char *tmp)
{
	if (*flag)
	{
		add_word(p, words, letter, tmp);
		*flag =0;
	}
}

int change_the_max_length_of_word(char ** tmp, int size_of_word, int letter)
{
	if (letter>=size_of_word) 	
	{
		char *t;
		size_of_word *=2;
		if (t = realloc(*tmp, size_of_word *sizeof(char)))
			*tmp = t;
		else
			error_f();
	}
	return size_of_word;
}

int change_the_max_length_of_list(char ***p, int max_capacity, int words)
{
	if (words>=max_capacity)
	{
		char ** t;
		max_capacity*=2;
		if (t = realloc(*p, max_capacity * sizeof(char*)))
			*p = t;
		else
			error_f();
	}
	return max_capacity;
}

void check_amper_flag(int * amper_flag, int * words, char **p, int * max_capacity, char * tmp)
{	
	if (*amper_flag)
	{
		*amper_flag = 0;
		(*words)++;
		*max_capacity = change_the_max_length_of_list(&p, *max_capacity, *words);
		add_word(p, *words, 1, tmp);
	}
}

void check_bar_flag(int * bar_flag, int * words, char **p, int * max_capacity, char * tmp)
{	
	if (*bar_flag)
	{
		*bar_flag = 0;
		(*words)++;
		*max_capacity = change_the_max_length_of_list(&p, *max_capacity, *words);
		add_word(p, *words, 1, tmp);
	}
}

int main()
{	
	int max_capacity = 1024;
	int size_of_word = 256;
	char ** p = (char **) malloc (max_capacity * sizeof(char *));
	if (!p)
		error_f();
	char * tmp = (char *) malloc (size_of_word * sizeof(char *));
	if (!tmp)
		error_f();
	int words = -1;
	int letter = 0;
	int buffer = getchar();
	int flag = 0;
	int amper_flag = 0;
	int bar_flag = 0;
	int quo_flag = 0;
	while (buffer!=EOF)
	{
		if (!quo_flag)
		{
			if (!isspace(buffer))
			{	
				if (buffer == '\'')
					quo_flag = 1;
				if (buffer == '\"')
					quo_flag = 2;
				else if (buffer == '&')
				{
					check_flag(&flag, p, words, letter, tmp);
					check_bar_flag(&bar_flag, &words, p, &max_capacity, tmp);
					tmp[amper_flag] = '&';
					letter = 1 + amper_flag;
					amper_flag = 1-amper_flag;
					if (amper_flag == 0)
					{
						++words;
						max_capacity = change_the_max_length_of_list(&p, max_capacity, words);
						add_word(p, words, 2, tmp);
					}	
				}
				else if (buffer == '|')
				{
					check_amper_flag(&amper_flag, &words, p, &max_capacity, tmp);
					check_flag(&flag, p, words, letter, tmp);
					tmp[bar_flag] = '|';
					letter = 1 + bar_flag;
					bar_flag = 1-bar_flag;
					if (bar_flag == 0)
					{
						++words;
						max_capacity = change_the_max_length_of_list(&p, max_capacity, words);
						add_word(p, words, 2, tmp);
					}	
				}
				else
				{
					check_amper_flag(&amper_flag, &words, p, &max_capacity, tmp);
					check_bar_flag(&bar_flag, &words, p, &max_capacity, tmp);
					if (buffer == ';')
					{
						check_flag(&flag, p, words, letter, tmp);
						++words;
						max_capacity = change_the_max_length_of_list(&p, max_capacity, words);
						tmp[0] = ';';
						add_word(p, words, 1, tmp);
						letter = 0;
					}
					else
					{
						if (flag)
							size_of_word = change_the_max_length_of_word (&tmp, size_of_word, letter);
						else
						{
							letter = 0;
							flag = 1;
							++words;
							max_capacity = change_the_max_length_of_list(&p, max_capacity, words);				
						}
						tmp[letter] = buffer;
						++letter;
					}
				}
			}
			else
			{
				check_flag(&flag, p, words, letter, tmp);
				check_amper_flag(&amper_flag, &words, p, &max_capacity, tmp);
				check_bar_flag(&bar_flag, &words, p, &max_capacity, tmp);
			}
		}
		else if (quo_flag == 1 && buffer!='\'' || quo_flag == 2 && buffer!='\"')
		{
			if (!flag)
			{
				flag =1;
				letter = 0;
				++words;
				max_capacity = change_the_max_length_of_list(&p, max_capacity, words);	
			}
			size_of_word = change_the_max_length_of_word (&tmp, size_of_word, letter);
			tmp[letter] = buffer;
			++letter;
		}
		else
			quo_flag = 0;
		buffer = getchar();
	}
	
	check_flag(&flag, p, words, letter, tmp);
	check_amper_flag(&amper_flag, &words, p, &max_capacity, tmp);
	check_bar_flag(&bar_flag, &words, p, &max_capacity, tmp);
	++words;
	qsort(p, words, sizeof(char*), cmp);
	free(tmp);
	int i;
	if(!quo_flag)
		for (i = 0; i<words; ++i)
		{
			printf("\"%s\"\n", p[i]);
			free(p[i]);
		}
	else
	{
		printf ("Quotation mark wasn't closed. ERROR.");
		for (i = 0; i<words; ++i)
			free(p[i]);
	}
	free(p);
	return 0;
}

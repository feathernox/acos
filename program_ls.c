#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <linux/limits.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

const int INITIAL_FILES = 256;
const int INITIAL_DIRECTORIES = 256;
const int INFORMATION = 16;
const int MAX_TIME_BUF = 64;

int cmp (const void *a, const void *b)
{
    return(strcmp(*(char**)a, *(char**)b));
}

void memory_reallocation (char *** memory_block, int * capacity, int * count, const char * name){
    if (*capacity == *count){
        char ** tmp = (char **) realloc(*memory_block, 2*(*capacity) * sizeof(char*));
        if (!tmp){
            perror("Cannot allocate memory.\n");
            exit(1);
        }
        *memory_block = tmp;
        (*capacity)*=2;
    }

    (*memory_block)[*count] = (char*) malloc(NAME_MAX);
    if(!(*memory_block)[*count]){
        perror("Cannot allocate memory.\n");
        exit(1);
    }
    strcpy((*memory_block)[*count], name);
    ++(*count);
}

void printf_error(){
    perror ("Cannot print to the output.\n");
    exit(1);
}

void list_files(char * directory_name){

    DIR *dir;
    struct dirent *entry;

    if (printf("%s:\n", directory_name)<0)
        printf_error();

    if(!(dir = opendir(directory_name))){
        if (access(directory_name, F_OK)<0)
        {
            perror("This directory does not exist.\n\n");
            return;
        }
        if (access(directory_name, R_OK)<0)
        {
            perror("Cannot access directory.\n\n");
            return;
        }
        perror("Cannot open the directory.\n");
        exit(1);
    }

    char ** files = (char **) malloc (INITIAL_FILES * sizeof (char*));
    int files_count = 0;
    int files_capacity = INITIAL_FILES;
    if (!files){
        perror("Cannot allocate memory.\n");
        exit(1);
    }

    errno = 0;
    while (entry = readdir(dir))
            memory_reallocation(&files, &files_capacity, &files_count, entry->d_name);

    if (errno){
        perror("Skipping the directory because of the error while reading.\n\n");
        int i;
        for (i = 0; i<files_count; ++i)
            free (files[i]);
        free(files);
        return;
    }
    if (closedir(dir)){
        perror("Cannot close directory.\n");
        exit(1);
    }

    qsort(files, files_count, sizeof(char*), cmp);

    int i;
    for (i = 0; i<files_count; ++i){
        if (strcmp(files[i], ".")!=0 && strcmp(files[i], "..") != 0){
            char buffer[PATH_MAX +1] = { 0 };
            struct stat buf;
            strcpy(buffer, directory_name);
            strcat(buffer, files[i]);
            if(lstat(buffer, &buf)<0)
                continue;
            char info[INFORMATION];

            if (S_ISDIR(buf.st_mode))
                info[0] = 'd';
            else if (S_ISLNK(buf.st_mode))
                strcpy(info, "l");
            else
                strcpy(info, "-");

            info[1] = ((buf.st_mode & S_IRUSR) ? 'r' : '-');
            info[2] = ((buf.st_mode & S_IWUSR) ? 'w' : '-');
            info[3] = ((buf.st_mode & S_IXUSR) ? 'x' : '-');
            info[4] = ((buf.st_mode & S_IRGRP) ? 'r' : '-');
            info[5] = ((buf.st_mode & S_IWGRP) ? 'w' : '-');
            info[6] = ((buf.st_mode & S_IXGRP) ? 'x' : '-');
            info[7] = ((buf.st_mode & S_IROTH) ? 'r' : '-');
            info[8] = ((buf.st_mode & S_IWOTH) ? 'w' : '-');
            info[9] = ((buf.st_mode & S_IXOTH) ? 'x' : '-');
            info[10] = 0;
            if (printf("%s %d ", info, buf.st_nlink)<0)
                printf_error();
            struct passwd *pw = getpwuid(buf.st_uid);
            if (pw){
                if (printf("%s ", pw->pw_name)<0)
                    printf_error();
            }
            else{
                if (printf("%d ", buf.st_uid)<0)
                    printf_error();
            }
            struct group *gr = getgrgid(buf.st_gid);
            if (gr){
                if (printf("%s ", gr->gr_name)<0)
                    printf_error();
            }
            else{
                if (printf("%d ", buf.st_gid)<0)
                    printf_error();
            }
            char timebuffer[MAX_TIME_BUF];
            struct tm * timeinfo;
            timeinfo = localtime (&buf.st_mtime);
            strftime(timebuffer, MAX_TIME_BUF, "%b %e %H:%M", timeinfo);
            if (printf("%d %s %s", buf.st_size, timebuffer, files[i])<0)
                printf_error();
            if (S_ISLNK(buf.st_mode)){
                char buffer[PATH_MAX + 1] = { 0 };
                char lnk[PATH_MAX + 1] = { 0 };
                strcpy(buffer, directory_name);
                strcat(buffer, files[i]);
                if (readlink(buffer, lnk, PATH_MAX)>=0)
                    if (printf(" -> %s", lnk)<0)
                        printf_error();
            }
            if (printf("\n")<0)
                printf_error();
        }
    }

    if (printf("\n")<0)
        printf_error();

    for (i = 0; i<files_count; ++i){
        if (strcmp(files[i], ".")!=0 && strcmp(files[i], "..") != 0){
            char buffer[PATH_MAX + 1] = { 0 };
            struct stat buf;
            strcpy(buffer, directory_name);
            strcat(buffer, files[i]);
            if(lstat(buffer, &buf)<0){
                free(files[i]);
                continue;
            }
            if (S_ISDIR(buf.st_mode)){
                strcat(buffer, "/");
                list_files(buffer);
            }
        }
        free(files[i]);
    }
    free(files);
}

int main(int argc, char *argv[]){
  if (argc>1){
      if (argv[1][strlen(argv[1])-1]=='/')
          list_files(argv[1]);
      else{
          char buffer[PATH_MAX] = { 0 };
          strcpy(buffer, argv[1]);
          buffer[strlen(argv[1])] = '/';
          list_files(buffer);
      }
  }
  else
      list_files("./");
  return 0;
}

﻿#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

const int BYTE = 256;
const int INITIAL_SIZE_OF_BUFFER = 256;

enum state_codes {INDEFINITE, SYMBOL, DOT, QUESTIONMARK, ASTERISK};

struct states{
    enum state_codes condition;
    char * symbols;
    int count_symbols;
    int marks;
};

void allocation_error(){
    perror ("Cannot allocate the memory.\n");
    exit(1);
}

void printf_error(){
    perror ("Cannot print to the output.\n");
    exit(1);
}
void memory_reallocation (char ** memory_block, int * capacity, int * count){
    if (*capacity == *count){
        char * tmp = (char *) realloc(*memory_block, 2*(*capacity));
        if (!tmp)
            allocation_error();
        *memory_block = tmp;
        (*capacity)*=2;
    }
    return;
}

void free_everything(struct states ** state_machine, int states_count){
    int i;
    for (i = 0; i<states_count; ++i){
        free(state_machine[i]->symbols);
        free(state_machine[i]);
    }
    free(state_machine);
}

int get_char_with_slash(const char * templ, int len, int * p){
    ++(*p);
    if (*p == len)
        return SCHAR_MAX+1;
    switch(templ[*p]){
        case 'a':
            return '\a';
        case 'b':
            return '\b';
        case 'f':
            return '\f';
        case 'n':
            return '\n';
        case 'r':
            return '\r';
        case 't':
            return '\t';
        case 'v':
            return '\v';
        default:
            return templ[*p];
    }
}

int make_machine(struct states ** state_machine, const char * templ){

    int len = strlen(templ);
    int p = 0;
    int states_count = 0;
    char indef_flag = 0;
    while (p<len){
        switch (templ[p]){

            case '.':{
                if (indef_flag && state_machine[states_count]->condition == INDEFINITE){
                    state_machine[states_count]->condition = SYMBOL;
                    ++states_count;
                    indef_flag = 0;
                }
                state_machine[states_count] = (struct states *) malloc (sizeof(struct states));
                if (!(state_machine[states_count]))
                    allocation_error();
                state_machine[states_count]->condition = DOT;
                ++states_count;
                ++p;
                break;
            }

            case '[':{
                if (indef_flag && state_machine[states_count]->condition == INDEFINITE){
                    indef_flag = 0;
                    state_machine[states_count]->condition = SYMBOL;
                    ++states_count;
                }
                char symbols[BYTE];
                int i;
                for (i = 0; i<BYTE; ++i)
                    symbols[i] = 0;
                char used_symbols = 0;
                ++p;
                char flag_end = 0;
                char flag_stroke = 2;
                int symb, end1;
                end1 = -SCHAR_MIN;
                while (p != len && !flag_end){

                    symb = -SCHAR_MIN;

                    if (templ[p]=='\\'){
                        symb+=get_char_with_slash(templ, len, &p);
                        if (symb == BYTE){
                            free_everything(state_machine, states_count);
                            return -1;
                        }
                    }

                    else{
                        if (templ[p]!='-'){
                            if (templ[p] == ']'){
                                if (flag_stroke == 1){
                                    free_everything(state_machine, states_count);
                                    return -1;
                                }
                                ++p;
                                flag_end = 1;
                                break;
                            }
                            symb+=templ[p];
                        }
                        else if (flag_stroke>0){
                            free_everything(state_machine, states_count);
                            return -1;
                        }
                        else{
                            flag_stroke = 1;
                            ++p;
                            continue;
                        }
                    }

                    if (!symbols[symb]){
                        symbols[symb] = 1;
                        ++used_symbols;
                    }

                    if (flag_stroke == 2)
                        flag_stroke = 0;
                    else if (flag_stroke ==1)
                    {
                        flag_stroke = 2;
                        if (end1>symb){
                            free_everything(state_machine, states_count);
                            return -1;
                        }
                        for (i = end1; i<=symb; ++i)
                            if (!symbols[i]){
                                symbols[i] = 1;
                                ++used_symbols;
                            }
                    }
                    if (flag_stroke == 0){
                        if (!symbols[symb]){
                            symbols[symb] = 1;
                            ++used_symbols;
                        }
                        end1 = symb;
                    }
                    ++p;
                }

                if (!flag_end){
                    free_everything(state_machine, states_count);
                    return -1;
                }
                state_machine[states_count] = (struct states *) malloc (sizeof(struct states));
                if (!(state_machine[states_count]))
                    allocation_error();
                state_machine[states_count]->condition = INDEFINITE;
                indef_flag = 1;
                state_machine[states_count]->symbols = (char *) malloc (used_symbols);
                if (!(state_machine[states_count]->symbols))
                    allocation_error();
                state_machine[states_count]->count_symbols = used_symbols;
                int cnt = 0;
                for (i = 0; i<BYTE; ++i)
                    if (symbols[i]){
                        int tmp = i + SCHAR_MIN;
                        state_machine[states_count]->symbols[cnt] = tmp;
                        ++cnt;
                    }
                break;
            }

            case '*':{
                if (!indef_flag){
                    free_everything(state_machine, states_count);
                    return -1;
                }
                state_machine[states_count]->condition = ASTERISK;
                indef_flag=0;
                state_machine[states_count]->marks =0;
                while (p < len && (templ[p]== '*' || templ[p] == '?')){
                    ++p;
                }
                ++states_count;
                break;
            }

            case '?':{
                if (!indef_flag){
                    free_everything(state_machine, states_count);
                    return -1;
                }
                state_machine[states_count]->condition = QUESTIONMARK;
                indef_flag = 0;
                state_machine[states_count]->marks = 0;
                while (p < len && (templ[p]== '*' || templ[p] == '?'))
                {
                    if (templ[p] == '*')
                        state_machine[states_count]->condition = ASTERISK;
                    ++p;
                    ++(state_machine[states_count]->marks);
                }
                ++states_count;
                break;
            }

            default:{
                if (indef_flag && state_machine[states_count]->condition == INDEFINITE){
                    state_machine[states_count]->condition = SYMBOL;
                    ++states_count;
                }

                int symb;
                if (templ[p]=='\\'){
                    symb = get_char_with_slash(templ, len, &p);
                    if (symb == SCHAR_MAX + 1){
                        free_everything(state_machine, states_count);
                        return -1;
                    }
                }
                else
                    symb = templ[p];
                ++p;
                state_machine[states_count] = (struct states *) malloc (sizeof(struct states));
                if (!(state_machine[states_count]))
                    allocation_error();
                state_machine[states_count]->symbols = (char*) malloc (sizeof(char));
                if (!(state_machine[states_count]->symbols))
                    allocation_error();
                state_machine[states_count]->symbols[0] = symb;
                state_machine[states_count]->count_symbols = 1;
                state_machine[states_count]->condition = INDEFINITE;
                indef_flag = 1;
            }

        }
    }

    if (indef_flag && state_machine[states_count]->condition == INDEFINITE){
        state_machine[states_count]->condition = SYMBOL;
        ++states_count;
    }

    return states_count;
}

char analyze_string (struct states ** state_machine, int state_start, int states_count, const char * string, int first_symbol, int len){
    if (state_start ==states_count)
        return 1;

    if (first_symbol == len)
        return 0;

    int i;
    switch (state_machine[state_start]->condition){
        case ASTERISK:{
            int sn = 0;
            for (i = 0; i<state_machine[state_start]->count_symbols; ++i)
                if (string[first_symbol] == state_machine[state_start]->symbols[i]){
                    sn = 1;
                    break;
                }
            if (sn)
                return analyze_string(state_machine, state_start+1, states_count, string, first_symbol+1, len) \
                        || analyze_string(state_machine, state_start+1, states_count, string, first_symbol, len) \
                        || analyze_string(state_machine, state_start, states_count, string, first_symbol+1, len); \
            else
                return analyze_string(state_machine, state_start+1, states_count, string, first_symbol, len);
        }
        case QUESTIONMARK:{
            int sn = 0;
            for (i = 0; i<state_machine[state_start]->count_symbols; ++i)
                if (string[first_symbol] == state_machine[state_start]->symbols[i]){
                    sn = 1;
                    break;
                }
            if (sn)
                return analyze_string(state_machine, state_start+1, states_count, string, first_symbol+1, len) \
                        || analyze_string(state_machine, state_start+1, states_count, string, first_symbol, len);
            else
                return analyze_string(state_machine, state_start+1, states_count, string, first_symbol, len);
        }

        case DOT:
            return analyze_string(state_machine, state_start+1, states_count, string, first_symbol+1, len);

        default:{
            for (i = 0; i<state_machine[state_start]->count_symbols; ++i)
                if (string[first_symbol] == state_machine[state_start]->symbols[i])
                    return analyze_string(state_machine, state_start+1, states_count, string, first_symbol+1, len);
            return 0;
        }
     }
}

void analyze_file(const char * name, struct states ** state_machine, int states_count, int fl){
    if (access(name, F_OK)<0){
        if (printf("%s: file doesn't exist.\n", name)<0)
            printf_error();
        return;
    }

    int fd = open(name, O_RDONLY);
    if (fd < 0){
        perror("Cannot open the file.\n");
        exit(1);
    }

    ssize_t ret;
    int capacity = INITIAL_SIZE_OF_BUFFER;
    char * buffer = (char *) malloc (capacity);
    if (!buffer)
        allocation_error();
    int count = 0;
    char ch;
    while ((ret = read (fd, &ch, 1))>0){
        memory_reallocation(&buffer, &capacity, &count);
        if (ch!='\n')
            buffer[count] = ch;
        else{
            buffer[count] = 0;
            int i;
            char res = 0;
            for (i = 0; i<count; ++i){
                res = analyze_string (state_machine, 0, states_count, buffer, i, count);
                if (res)
                    break;
            }
            if (res){
                if (fl){
                    if (printf("%s: %s\n", name, buffer)<0)
                        printf_error();
                }
                else{
                    if (printf("%s\n", buffer)<0)
                        printf_error();
                }

            }
            memset(buffer, 0, count);
            count = -1;
        }
        ++count;
    }

    if (ret<0){
        perror("Cannot read the file.\n");
        exit(1);
    }

    if (count!=0){
        int i;
        char res = 0;
        for (i = 0; i<count; ++i){
            res = analyze_string (state_machine, 0, states_count, buffer, i, count);
            if (res)
                break;
        }
        if (res){
            if (fl){
                if (printf("%s: %s\n", name, buffer)<0)
                    printf_error();
            }
            else{
                if (printf("%s\n", buffer)<0)
                    printf_error();
            }
        }
    }

    char res = analyze_string (state_machine, 0, states_count, buffer, 0, count);
    if (res)
        if (printf("%s\n", buffer)<0)
            printf_error();
    free(buffer);

    if (close (fd)<0){
        perror("Failure of closing file.\n");
        exit(1);
    }
}

void bufferize(struct states ** state_machine, int states_count){
    ssize_t ret;
    int capacity = INITIAL_SIZE_OF_BUFFER;
    char * buffer = (char *) malloc (capacity);
    if (!buffer)
        allocation_error();
    int count = 0;
    char ch;
    while ((ret = read (STDIN_FILENO, &ch, 1))>0){
        memory_reallocation(&buffer, &capacity, &count);
        if (ch!='\n')
            buffer[count] = ch;
        else{
            buffer[count] = 0;
            int i;
            char res = 0;
            for (i = 0; i<count; ++i){
                res = analyze_string (state_machine, 0, states_count, buffer, i, count);
                if (res)
                    break;
            }
            if (res){
                if (printf("%s\n", buffer)<0)
                    printf_error();
            }
            memset(buffer, 0, count);
            count = -1;
        }
        ++count;
    }

    if (ret<0){
        perror("Cannot read from the input.\n");
        exit(1);
    }

    if (count!=0){
        int i;
        char res = 0;
        for (i = 0; i<count; ++i){
            res = analyze_string (state_machine, 0, states_count, buffer, i, count);
            if (res)
                break;
        }
        if (res){
            if (printf("%s\n", buffer)<0)
                printf_error();
        }
    }
    free(buffer);
}

int main(int argc, char *argv[]){
    if (argc<2){
        if(printf("No template is pointed.\n")<0)
            printf_error();
        return 0;
    }
    struct states ** state_machine;

    state_machine = (struct states **) malloc (strlen(argv[1])*sizeof(struct states *));
    if (!state_machine)
        allocation_error();

    int states_count = make_machine(state_machine, argv[1]);
    if (states_count<0)
    {
        if (printf("Wrong template.\n")<0)
            printf_error();
        return 0;
    }
    /*int i;
    for (i = 0; i<states_count; ++i){
        switch (state_machine[i]->condition){
            case ASTERISK:{
                printf("* %d: %s\n", state_machine[i]->marks, state_machine[i]->symbols);
                break;
            }
            case QUESTIONMARK:{
                printf("? %d: %s\n", state_machine[i]->marks, state_machine[i]->symbols);
                break;
            }
            case SYMBOL:{
                printf("s: %s\n", state_machine[i]->symbols);
                break;
            }
            case DOT:{
                printf(".\n");
                break;
            }
            default:
                printf("INDEFINITE.\n");
         }
    }*/

    if (argc == 2){
        bufferize(state_machine, states_count);
    }
    else{
        int i;
        for (i = 2; i<argc; ++i)
            analyze_file(argv[i], state_machine, states_count, argc-3);
    }
    free_everything(state_machine, states_count);
    return 0;
}

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned long long int prime_t;

int threads_num;
pthread_t * threads;
struct segment * arg_threads;
prime_t sieve_size = 32768;
char * sieve_of_eratosthenes;

struct segment{
    prime_t n;
    prime_t begin;
    prime_t end;
};

void malloc_error(){
    perror("Unable to allocate memory.");
    exit(1);
}

void set_threads_num(const char * arg){
    int check_threads_num = atoi(arg);
    if (check_threads_num < 1){
        perror ("Wrong argument for number of threads.\n");
        exit(1);
    }
    else{
        threads_num = check_threads_num;
    }
}

void set_sieve_size(const char * arg){
    int check_sieve_size = atoi(arg);
    if (check_sieve_size < 1){
        perror ("Wrong argument for sieve size.\n");
        exit(1);
    }
    sieve_size = check_sieve_size+1;
}

void * analyze_segment(void * arg){
    struct segment seg = * (struct segment *) arg;
    prime_t i, j;
    for (i = 2; i <= seg.n; ++i){
        if (!sieve_of_eratosthenes[i])
            continue;
        //находим первое число после начала, которое делится на i
        prime_t new_begin = ((seg.begin + i - 1)/i) * i;
        for (j = new_begin; j <= seg.end; j += i){
            sieve_of_eratosthenes[j] = 0;
        }
    }
    printf("[%llu; %llu] ", seg.begin, seg.end);
    return NULL;
}

void limited_loop(){
    prime_t i;
    for (i = 2; i < sieve_size-1; i *= i){
        prime_t end = i*i;
        if (end > sieve_size-1)
            end = sieve_size-1;
        /*на тот случай, если чисел, которые надо обработать
         * меньше числа нитей*/
        prime_t real_threads_num = end-i;
        if (real_threads_num > threads_num)
            real_threads_num = threads_num;
        //число нитей, который обрабатывают на 1 больше других
        prime_t full_length = end - i;
        prime_t s_threads_num = full_length % real_threads_num;
        prime_t end_segment = i+1;
        prime_t j;
        for (j = 0; j <real_threads_num; ++j){
            arg_threads[j].n = i;
            arg_threads[j].begin = end_segment;
            prime_t length = full_length / real_threads_num;
            if (j < s_threads_num)
                ++length;
            end_segment+=length-1;
            arg_threads[j].end = end_segment;
            ++end_segment;

            if (pthread_create(threads + j, NULL, analyze_segment, arg_threads+j)!=0){
                perror("Failed to create the thread.\n");
                exit(1);
            }
        }
        void * wait_thr;
        for (j = 0; j < real_threads_num; ++j){
            if (pthread_join(threads[j], &wait_thr) != 0){
                perror("Failed to join the thread.\n");
                exit(1);
            }
        }

       for (j = i + 1; j!= end; ++j)
            if (sieve_of_eratosthenes[j] == 1)
                printf("%llu\n", j);
    }
}

void unlimited_loop(){
    prime_t i;
    for (i = 2;; i *= i){
        prime_t end = i*i;
        if (end > sieve_size-1){
            char * tmp = realloc(sieve_of_eratosthenes, end + 1);
            if (!tmp)
                malloc_error();
            sieve_of_eratosthenes = tmp;
            memset(sieve_of_eratosthenes+sieve_size, 1, end + 1 - sieve_size);
            sieve_size = end + 1;
        }
        prime_t real_threads_num = end-i;
        if (real_threads_num > threads_num)
            real_threads_num = threads_num;
        //число нитей, который обрабатывают на 1 больше других
        prime_t full_length = end - i;
        prime_t s_threads_num = full_length % real_threads_num;
        prime_t end_segment = i+1;
        prime_t j;
        for (j = 0; j <real_threads_num; ++j){
            arg_threads[j].n = i;
            arg_threads[j].begin = end_segment;
            prime_t length = full_length / real_threads_num;
            if (j < s_threads_num)
                ++length;
            end_segment+=length-1;
            arg_threads[j].end = end_segment;
            ++end_segment;

            if (pthread_create(threads + j, NULL, analyze_segment, arg_threads+j)!=0){
                perror("Failed to create the thread.\n");
                exit(1);
            }
        }
        void * wait_thr;
        for (j = 0; j < real_threads_num; ++j){
            if (pthread_join(threads[j], &wait_thr) != 0){
                perror("Failed to join the thread.\n");
                exit(1);
            }
        }

       for (j = i + 1; j!= end; ++j)
            if (sieve_of_eratosthenes[j] == 1)
                printf("%llu\n", j);
    }
}

int main(int argc, const char * argv[]){
    if (argc == 1){
        perror ("Not enough arguments.\n");
        exit(1);
    }
    else
        set_threads_num(argv[1]);
    if (argc > 2)
        set_sieve_size(argv[2]);

    sieve_of_eratosthenes = (char *) malloc(sieve_size);
    if (!sieve_of_eratosthenes)
        malloc_error();
    memset(sieve_of_eratosthenes, 1, sieve_size);
    sieve_of_eratosthenes[0] = 0;
    sieve_of_eratosthenes[1] = 0;

   threads = (pthread_t *) malloc(sizeof(pthread_t) * threads_num);
   if (!threads)
       malloc_error();

   arg_threads = (struct segment *) malloc (sizeof(struct segment)*threads_num);
   if (!arg_threads)
       malloc_error();

    if (sieve_size -1 >= 2)
        printf("2\n");\
    if (argc > 2)
        limited_loop();
    else
        unlimited_loop();
    free(arg_threads);
    free(threads);
    free(sieve_of_eratosthenes);

    return 0;
}

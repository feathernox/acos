#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>

#define DEFAULT_MAX_ARGS 8

char ***args;
int * count_args;

void allocation_error(){
    perror("Allocation error.");
    exit(1);
}

int main(int argc, char **argv) {
    if (argc <= 1){
        perror("No arguments.\n");
        exit(1);
    }
    //parse the arguments
    args = (char ***) malloc ((argc-1) * sizeof(char **));
    if (!args)
        allocation_error();
    count_args = (int *) malloc ((argc-1)* sizeof(int));
    if (!count_args)
        allocation_error();
    int i;
    for (i = 0; i <argc-1; ++i){
        const char * analyze_this = argv[i+1];
        int this_length = strlen(analyze_this);
        args[i] = (char **) malloc (sizeof(char *)* DEFAULT_MAX_ARGS);
        int capacity = DEFAULT_MAX_ARGS;
        int arg_length = 0;
        int n;
        for (n = 0; n <= this_length; ++n){
            if ((isspace(analyze_this[n]) || analyze_this[n] == 0) && arg_length > 0){
                args[i][count_args[i]] = (char *) malloc(sizeof(char) * (arg_length +1));
                if (!args[i][count_args[i]])
                    allocation_error();
                args[i][count_args[i]][arg_length] = 0;
                memcpy(args[i][count_args[i]], analyze_this + n - arg_length, arg_length);
                ++count_args[i];
                if (count_args[i] >= capacity){
                    char ** tmp = realloc (args[i], 2*capacity * sizeof(char *));
                    if (!tmp)
                        allocation_error();
                    args[i] = tmp;
                    capacity*=2;
                }
                arg_length = 0;
            }
           if(!isspace(analyze_this[n])){
               ++arg_length;
           }
        }
        if (count_args[i] == 0){
            perror("Empty argument.\n");
            exit(1);
        }
        args[i][count_args[i]] = NULL;
    }

    //do piping
    int n;
    int ** fd = (int **) malloc ((argc-2) * sizeof(int *));
    for (i = 0; i < argc-2; ++i){
        fd[i] = (int *) malloc(2*sizeof(int));
        pipe(fd[i]);
    }
    for (i = 0; i < argc - 1; ++i){
        int k = fork();
        if (k<0){
            perror("fork");
            exit(1);
        }
        if (k == 0){
            if (i!= 0)
                dup2(fd[i-1][0], 0);
            if (i!=argc-2)
                dup2(fd[i][1], 1);
            int j;
            for (j = 0; j < argc-2; j++){
                close (fd[j][0]);
                close (fd[j][1]);
            }
            execvp(args[i][0], args[i]);
            perror(args[i][0]);
            exit(1);
        }
    }
    for (i = 0; i < argc-2; ++i){
        close(fd[i][0]);
        close(fd[i][1]);
    }
    for (i = 0; i <argc-1; ++i){
        wait(&n);
    }
    for (i = 0; i < argc-2; ++i){
        free(fd[i]);
    }
    free(fd);
    //clear arguments
    for (i = 0; i <argc-1; ++i){
        int j;
        for (j = 0; j<count_args[i]; ++j){
            free (args[i][j]);
        }
        free(args[i]);
    }
    free(args);
    free(count_args);
}

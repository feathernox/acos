#include<stdio.h>
#include<stdlib.h>
#include<strings.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netdb.h>

#define TCP_PORT_NUMBER "8080"
#define BACKLOG 5
#define INITIAL_CAPACITY 1024

void allocation_error(){
    perror ("Cannot allocate the memory.\n");
    exit(1);
}

int doprocessing(int connection_descriptor){
    int i,ret;
    char ch;
    const int MAX_ARGS = 3;
    int size[MAX_ARGS];
    int pos[MAX_ARGS];
    for (i = 0; i<MAX_ARGS; ++i){
        size[i] = INITIAL_CAPACITY;
        pos[i] = 0;
    }
    char ** buffer = (char **) malloc (MAX_ARGS * sizeof(char *));
    if (!buffer)
        allocation_error();
    for (i = 0 ; i!= MAX_ARGS; ++i){
        buffer[i] = (char *) malloc (size[i]);
        if (!buffer[i])
            allocation_error();
    }
    dup2(connection_descriptor, STDOUT_FILENO);
    dup2(connection_descriptor, STDIN_FILENO);
    dup2(connection_descriptor, STDERR_FILENO);
    close(connection_descriptor);
    i = 0;
    while ((ret = read(STDIN_FILENO, &ch, 1))>0){
        if (ch == '\n')
            break;
        if (ch == ':' && i<2){
            buffer[i][pos[i]] = 0;
            ++i;
            continue;
        }
        buffer[i][pos[i]] = ch;
        ++pos[i];
        if (pos[i] >= size[i]){
            char* tmp = (char *) realloc (buffer[i], 2* size[i]);
            if (!tmp)
                allocation_error();
            buffer[i] = tmp;
            size[i]*=2;
        }
    }
    buffer[pos[i]] = 0;
    if (ret < 0){
        perror ("Error reading from socket");
        exit(1);
    }
    if (strcmp(buffer[0], "ls") == 0)
        if (pos[1]!=0)
            execlp("ls", "ls", buffer[1], NULL);
    if (strcmp(buffer[0], "grep") == 0)
        if (pos[1]!=0 && pos[2]!=0)
            execlp("grep", "grep", buffer[1], buffer[2], NULL);
    for (i = 0; i!= MAX_ARGS; ++i){
        free(buffer[i]);
    }
    free(buffer);
	exit(1);
}

int main(){
    struct addrinfo hints;
    struct addrinfo * server_info;
    bzero(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if (getaddrinfo(NULL,TCP_PORT_NUMBER,&hints,&server_info)!=0){
        perror("Error on getting information about server.\n");
        exit(1);
    }

    int listen_descriptor = 0;
    struct addrinfo * p;
    for(p = server_info; p!= NULL; p = p->ai_next){
        if ((listen_descriptor = socket(p->ai_family, p->ai_socktype, p->ai_protocol))<0){
            perror("Error on connecting to this address.\n");
            continue;
        }

        if (bind(listen_descriptor, p->ai_addr, p->ai_addrlen) == 0){
            break;
        }

        close(listen_descriptor);
    }

    if (p == NULL){
        perror("Error on connecting to server.\n");
        exit(1);
    }

    freeaddrinfo(server_info);

    if (listen(listen_descriptor, BACKLOG)<0){
        perror("Error on listening.\n");
        exit(1);
    }

    int pid = 0;
    int connection_descriptor = 0;
    while(1){
        connection_descriptor = accept(listen_descriptor, (struct sockaddr *) NULL, NULL);
        if (connection_descriptor<0){
            perror("Error on accepting.\n");
            exit(1);
        }

        pid = fork();
        if (pid < 0){
            perror("Error on forking.\n");
            exit(1);
        }

        if (pid == 0){
            close(listen_descriptor);
            doprocessing(connection_descriptor);
        }

        else{
            close(connection_descriptor);
        }

    }
    return 0;
}
